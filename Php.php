<?php
/* STRING PHP */
$string = "Jajang Mahrul Subang";
echo ("Jumlah String: " . strlen($string)) . PHP_EOL;
echo ("Jumlah Kata: " . str_word_count($string)) . PHP_EOL;

$string1 = "Jajang Mahrul Subang";
echo "Kata pertama: " . substr($string1, 0, 6) . PHP_EOL;
echo "Kata kedua: " . substr($string1, 6, 7) . PHP_EOL;

$string2 = "Jajang Mahrul Subang";
echo ("Before replace " . $string2) . PHP_EOL;
echo ("After replace " . str_replace("Subang", "Purwakarta", $string2)) . PHP_EOL;

$string3 = "Jajang Mahrul Subang";
print_r(explode(" ", $string3)) . PHP_EOL;

$array = array('Jajang', 'Mahrul', 'Subang');
echo ("array to string: " . implode(" ", $array)) . PHP_EOL;

$string4 = "Jajang Mahrul Subang";
echo strtolower($string4) . PHP_EOL;
echo strtoupper($string4) . PHP_EOL;


/* ARRAY PHP */

$Kids = array("Mike", "Dustin", "Will", "Lucas", "Max", "Eleven");
$Adults = array("Hopper", "Nancy",  "Joyce", "Jonathan", "Murray");

var_dump($Kids);
var_dump($Adults);
